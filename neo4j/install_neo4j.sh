#!/bin/bash -

if [ -d "data" ]; then
    echo "/data exist"
else
    mkdir data
fi
if [ -d "conf" ]; then
    echo "/conf exist"
else
    mkdir conf
fi

cd conf
echo "dbms.security.procedures.unrestricted=algo.*" > neo4j.conf

docker run -it --rm \
--env=NEO4J_AUTH=none \
--env 'NEO4JLABS_PLUGINS=["apoc", "graph-algorithms"]'  \
--publish=7474:7474 --publish=7687:7687 \
--volume=$OLDPWD/data:/data \
--volume=$OLDPWD/conf:/conf \
neo4j:3.5.11