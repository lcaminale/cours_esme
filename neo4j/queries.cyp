// trouve les voyage de A vers B en passant par C et qui n'ont pas de direct A -> B
Match (a),(b),(c) where (a)-[]->(c)-[]->(b) and not (a)-[]->(b) and not  a.name=b.name return a,b,c LIMIT 25

// retourne tout les noeuds et les relations
MATCH (n) RETURN n limit 25


MATCH (start:Loc{name:'FRLYS'}), (end:Loc{name:'FRTNE'})
CALL algo.shortestPath.stream(start, end, 'price')
YIELD nodeId, cost
RETURN algo.asNode(nodeId).name AS name, cost